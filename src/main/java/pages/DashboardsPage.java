package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;

import java.util.List;

public class DashboardsPage extends HomePage {
    private String GENERIC_CHARACTERISTIC_ROW_XPATH = "//td[1]/p/text()";

    public DashboardsPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(css = ".x_title>h2")
    private WebElement processesElement;

    @FindBy(css = ".x_title>h2")
    public List<WebElement> processesElementList;

    @FindBy(xpath = "//td[1]/p")
    public List<WebElement> characteristicElementList;

    public DashboardsPage assertProcessesHeader() {
        Assert.assertEquals(processesElement.getText(), "DEMO PROJECT");
        return this;
    }

    public DashboardsPage assertProcessesUrl(String pageUrl) {
        Assert.assertEquals(driver.getCurrentUrl(), pageUrl);
        return this;
    }

    public DashboardsPage assertAddedProccesIsShown(String expProcess) {
        boolean doesErrorExists = processesElementList
                .stream()
                .anyMatch(validationError -> validationError.getText().equals(expProcess));
        Assert.assertTrue(doesErrorExists);
        return this;
    }

    public DashboardsPage assertAddedCharacteristicIsShown(String expCharacteristic) {
        boolean doesErrorExists = characteristicElementList
                .stream()
                .anyMatch(validationError -> validationError.getText().equals(expCharacteristic));
        Assert.assertTrue(doesErrorExists);
        return this;
    }
}
