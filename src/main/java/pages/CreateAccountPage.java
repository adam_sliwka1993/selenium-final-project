package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
//import static org.assertj.core.api.Assertions.*;


import java.util.List;

public class CreateAccountPage {
    protected WebDriver driver;

    public CreateAccountPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "#Email")
    private WebElement emailTxt;

    @FindBy(css = "#Password")
    private WebElement passwordTxt;

    @FindBy(css = "#ConfirmPassword")
    private WebElement confirmPasswordTxt;

    @FindBy(css = "button[type=submit]")
    private WebElement registerBtn;

    @FindBy(css = "#ConfirmPassword-error")
    private WebElement confirmPasswordError;

    @FindBy(css = ".validation-summary-errors>ul>li")
    public List<WebElement> validationErrors;

    public CreateAccountPage typeEmail(String email) {
        emailTxt.clear();
        emailTxt.sendKeys(email);
        return this;
    }

    public CreateAccountPage typePassword(String password) {
        passwordTxt.clear();
        passwordTxt.sendKeys(password);
        return this;
    }

    public CreateAccountPage typeConfirmPassword(String password) {
        confirmPasswordTxt.clear();
        confirmPasswordTxt.sendKeys(password);
        return this;
    }

    public HomePage registerButton() {
        registerBtn.click();
        return new HomePage(driver);
    }

    public CreateAccountPage submitRegisterButtonWithFailure() {
        registerBtn.click();
        return this;
    }

    public CreateAccountPage assertConfirmPasswordErrorIsShown(String expError) {
        Assert.assertTrue(confirmPasswordError.isDisplayed());
        Assert.assertEquals(confirmPasswordError.getText(), expError);
        return this;
    }

    public CreateAccountPage assertvalidationErrorsIsShown(String expError) {
        boolean doesErrorExists = validationErrors
                .stream()
                .anyMatch(validationError -> validationError.getText().equals(expError));
        Assert.assertTrue(doesErrorExists);

//        assertThat(validationErrors.stream().anyMatch(validationError -> validationError.getText().equals(expError)));

        return this;
    }
}