package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

public class ReportPage {
    protected WebDriver driver;

    public ReportPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    @FindBy(xpath = "//td[text()='Mean (x)']/../td[2]")
    private WebElement meanCell;

    @FindBy(xpath = "//td[text()='Standard deviation (s)']/../td[2]")
    private WebElement standardDeviationCell;

    @FindBy(xpath = "//td[text()='Performance index (Pp)']/../td[2]")
    private WebElement performanceIndexCell;

    @FindBy(xpath = "//td[text()='Lower process performance index (Ppl)']/../td[2]")
    private WebElement lowerProcessPerformanceIndexCell;

    @FindBy(xpath = "//td[text()='Upper process performance index (Ppu)']/../td[2]")
    private WebElement upperProcessPerformanceIndexCell;

    @FindBy(xpath = "//td[text()='Process performance index (Ppk)']/../td[2]")
    private WebElement processPerformanceIndexCell;

    @FindBy(xpath = "//td[text()='Lower Specification Limit (LSL)']/../td[2]")
    private WebElement lowerSpecificationLimitCell;

    @FindBy(xpath = "//td[text()='Upper Specification Limit (USL)']/../td[2]")
    private WebElement upperSpecificationLimitCell;

    public ReportPage asserLowerSpecificationLimit(String expMean){
        Assert.assertEquals(lowerSpecificationLimitCell.getText(), expMean);
        return this;
    }

    public ReportPage asserUpperSpecificationLimit(String expMean){
        Assert.assertEquals(upperSpecificationLimitCell.getText(), expMean);
        return this;
    }

    public ReportPage assertMean(String expMean){
        Assert.assertEquals(meanCell.getText(), expMean);
        return this;
    }

    public ReportPage assertStandardDeviationCell(String expMean){
        Assert.assertEquals(standardDeviationCell.getText(), expMean);
        return this;
    }

    public ReportPage assertPerformanceIndexCell(String expMean){
        Assert.assertEquals(performanceIndexCell.getText(), expMean);
        return this;
    }

    public ReportPage assertLowerProcessPerformanceIndexCell(String expMean){
        Assert.assertEquals(lowerProcessPerformanceIndexCell.getText(), expMean);
        return this;
    }

    public ReportPage assertUpperPerformanceIndexCell(String expMean){
        Assert.assertEquals(upperProcessPerformanceIndexCell.getText(), expMean);
        return this;
    }

    public ReportPage assertProcessPerformanceIndexCellCell(String expMean){
        Assert.assertEquals(processPerformanceIndexCell.getText(), expMean);
        return this;
    }
}

