import org.testng.annotations.Test;
import pages.LoginPage;

public class Test_2_Niepoprawna_Rejestracja_Niezgodne_Hasla extends SeleniumBaseTest {
    String actualError = "The password and confirmation password do not match.";

    @Test
    public void incorrectRegisterNoSamePassword() {
        new LoginPage(driver)
                .goToRegisterPage()
                .typeEmail("adamsliwka1@gmail.com")
                .typePassword("Insignia#11")
                .typeConfirmPassword("Insignia$11")
                .submitRegisterButtonWithFailure()
                .submitRegisterButtonWithFailure()
                .assertConfirmPasswordErrorIsShown(actualError)
                .assertvalidationErrorsIsShown(actualError);
    }
}
