import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Test_7_Poprawne_Dodanie_Procesu_Wynik_Strona_Dashboard extends SeleniumBaseTest{

    @Test
    public void addProcessTest() {
        String processName = UUID.randomUUID().toString().substring(0, 10);
        String processDescription = "Proces obróbki materiału z kamienia";
        String processNotes = "Przygowotać ostrzę stalowe";

        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                .clickAddProcess()
                .typeName(processName)
                .typeDescription(processDescription)
                .typeNotes(processNotes)
                .submitCreate()
                .goToDashboards()
                .assertAddedProccesIsShown(processName);
    }
}
