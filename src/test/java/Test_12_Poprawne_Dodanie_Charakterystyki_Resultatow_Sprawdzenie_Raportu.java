import org.junit.Ignore;
import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Test_12_Poprawne_Dodanie_Charakterystyki_Resultatow_Sprawdzenie_Raportu extends SeleniumBaseTest {

    @Test(enabled = false)
    public void reportTest() {
        String processName = "DEMO PROJECT";
        String characteristicName = UUID.randomUUID().toString().substring(0, 10);
        String number = UUID.randomUUID().toString().substring(0, 3);
        String lsl = "45";
        String usl = "48";
        String sampleName = "Test próbny" + number;
        String results = "78.0;45.0";
        String expMean = "61.5000";
        String expStandardDeviation = "23.3345";
        String expPerformanceIndex = "0.0214";
        String expLowerProcessPerformanceIndex = "0.2357";
        String expUpperProcessPerformanceIndex = "-0.1928";
        String expProcessPerformanceIndex = "-0.1928";

        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToCharacteristics()
                .clickAddCharacteristic()
                .selectProcess(processName)
                .typeName(characteristicName)
                .typeLsl(lsl)
                .typeUsl(usl)
                .submitCreate()
                .assertCharacteristic(characteristicName, lsl, usl, "")
                .goToResults(characteristicName)
                .clickAddResults()
                .typeSampleName(sampleName)
                .typeResults(results)
                .submitCreate()
                .backToCharacteristics()
                .goToReport(characteristicName)
                .asserLowerSpecificationLimit(lsl + ".0000")
                .asserUpperSpecificationLimit(usl + ".0000")
                .assertMean(expMean)
                .assertStandardDeviationCell(expStandardDeviation)
                .assertPerformanceIndexCell(expPerformanceIndex)
                .assertLowerProcessPerformanceIndexCell(expLowerProcessPerformanceIndex)
                .assertUpperPerformanceIndexCell(expUpperProcessPerformanceIndex)
                .assertProcessPerformanceIndexCellCell(expProcessPerformanceIndex);
    }
}
