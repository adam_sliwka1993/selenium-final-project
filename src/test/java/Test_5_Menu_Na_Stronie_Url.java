import org.testng.annotations.Test;
import pages.LoginPage;

public class Test_5_Menu_Na_Stronie_Url extends SeleniumBaseTest {

    @Test
    public void menuTest() {
        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .assertWelcomeElementIsShown(config.getApplicationUrl())
                .goToProcesses()
                .assertProcessesHeader()
                .assertProcessesUrl(config.getApplicationUrl() + "Projects")
                .goToCharacteristics()
                .assertProcessesHeader()
                .assertProcessesUrl(config.getApplicationUrl() + "Characteristics")
                .goToDashboards()
                .assertProcessesHeader()
                .assertProcessesUrl(config.getApplicationUrl());
    }
}
