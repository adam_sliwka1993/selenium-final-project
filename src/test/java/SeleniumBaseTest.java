import config.Config;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class SeleniumBaseTest {
    protected WebDriver driver;
    protected Config config = new Config();
//    protected static final String APPLICATION_URL

    @BeforeMethod
    public void baseBeforeMethod() throws MalformedURLException {
        setTheBrowser("deafult");
    }

    @AfterMethod
    public void takeScreenShot(ITestResult testResult) throws IOException {
        if (testResult.getStatus() == ITestResult.FAILURE) {
            snapScreenShot("failure", testResult.getName());
        }
        driver.quit();
    }

    private void snapScreenShot(String folder, String name) throws IOException {
        File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
        String fileName = "\\Snapshots\\" + folder + "\\" + name + "_" +
                new SimpleDateFormat("yyyy-MM-dd HH-mm-ss").format(new Date()) + ".jpg";
        String filePath = System.getProperties().get("user.dir") + fileName;
        FileUtils.copyFile(scrFile, new File(filePath));
    }

    public void setTheBrowser(String browserName) throws MalformedURLException {
        if (browserName.contains("deafult")) {
            System.setProperty("webdriver.chrome.driver", config.getChromeDriverPath());
            driver = new ChromeDriver();
        }
        if (browserName.contains("chrome")) {
            System.setProperty("webdriver.chrome.driver", config.getChromeDriverPath());
            ChromeOptions chromeOptions = new ChromeOptions();
            String node = "http://localhost:4444/wd/hub";
            driver = new RemoteWebDriver(new URL(node), chromeOptions);
        }
        if (browserName.contains("firefox")) {
            System.setProperty("webdriver.gecko.driver", config.getFirefoxDriverPath());
            FirefoxOptions options = new FirefoxOptions();
            options.addPreference("network.proxy.type", 0);
            driver = new RemoteWebDriver(options);
        }
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        driver.get(config.getApplicationUrl());
    }


}
