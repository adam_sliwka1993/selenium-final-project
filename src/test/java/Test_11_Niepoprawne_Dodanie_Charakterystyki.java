import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Test_11_Niepoprawne_Dodanie_Charakterystyki extends SeleniumBaseTest {

    @Test
    public void addCharacteristicNegative() {
        String characteristicName = UUID.randomUUID().toString().substring(0, 10);
        String lsl = "8";
        String usl = "";

        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToCharacteristics()
                .clickAddCharacteristic()
                .typeName(characteristicName)
                .typeLsl(lsl)
                .typeUsl(usl)
                .submitCreateWithFailure()
                .assertProcessError("The value 'Select process' is not valid for ProjectId.")
                .assertUslError("The value '' is invalid.")
                .backToList()
                .assertProcessIsNotShown(characteristicName);
    }
}
