import org.testng.annotations.Test;
import pages.LoginPage;

public class Test_4_Poprawne_Logowanie extends SeleniumBaseTest {

    @Test
    public void correctLoginTest() {
        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .assertWelcomeElementIsShown(config.getApplicationUrl());
    }
}
