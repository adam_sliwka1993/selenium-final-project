import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Test_6_Poprawne_Dodanie_Procesu_Wynik_Tabela_Processes extends SeleniumBaseTest {

    @Test
    public void addProcessTest() {
        String processName = UUID.randomUUID().toString().substring(0, 10);
        String processDescription = "Proces obróbki materiału ze stali";
        String processNotes = "Przygowotać ostrzę diamentowe";

        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                .clickAddProcess()
                .typeName(processName)
                .typeDescription(processDescription)
                .typeNotes(processNotes)
                .submitCreate()
                .assertProcess(processName, processDescription, processNotes);
    }
}
