import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Test_9_Poprawne_Dodanie_Charakterystyki_Wynik_Tabela_Characteristic extends SeleniumBaseTest {

    @Test
    public void addCharacteristic() {
        String processName = "DEMO PROJECT";
        String characteristicName = UUID.randomUUID().toString().substring(0, 10);
        String lsl = "233";
        String usl = "232";

        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToCharacteristics()
                .clickAddCharacteristic()
                .selectProcess(processName)
                .typeName(characteristicName)
                .typeLsl(lsl)
                .typeUsl(usl)
                .submitCreate()
                .assertCharacteristic(characteristicName, lsl, usl, "");
    }
}
