import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Test_10_Poprawne_Dodanie_Charakterystyki_Wynik_Strona_Dashboard extends SeleniumBaseTest {

    @Test
    public void addCharacteristic() {
        String processName = "DEMO PROJECT";
        String characteristicName = UUID.randomUUID().toString().substring(0, 10);
        String lsl = "123";
        String usl = "100";

        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToCharacteristics()
                .clickAddCharacteristic()
                .selectProcess(processName)
                .typeName(characteristicName)
                .typeLsl(lsl)
                .typeUsl(usl)
                .submitCreate()
                .goToDashboards()
                .assertAddedCharacteristicIsShown(characteristicName);
    }
}
