import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Test_1_Poprawna_Rejestracja extends SeleniumBaseTest {

    @Test
    public void correctRegisterTest() {
        String randomPartEmail = UUID.randomUUID().toString().substring(0, 5);

        new LoginPage(driver)
                .goToRegisterPage()
                .typeEmail("hawaje" + randomPartEmail + "@test.com")
                .typePassword("Honolulu1!")
                .typeConfirmPassword("Honolulu1!")
                .registerButton()
                .assertWelcomeElementIsShown(config.getApplicationUrl());
    }
}
