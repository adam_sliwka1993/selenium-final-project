import org.testng.annotations.Test;
import pages.LoginPage;

import java.util.UUID;

public class Test_8_Niepoprawne_Dodanie_Procesu extends SeleniumBaseTest {

    @Test
    public void addProcessWithFailureTest() {
        String longProcessName = UUID.randomUUID().toString().substring(0, 35);
        String expErrorMessage = "The field Name must be a string with a minimum length of 3 and a maximum length of 30.";

        new LoginPage(driver)
                .typeEmail(config.getApplicationUser())
                .typePassword(config.getApplicationPassword())
                .submitLogin()
                .goToProcesses()
                .clickAddProcess()
                .typeName(longProcessName)
                .submitCreateWithFailure()
                .assertProcessNameError(expErrorMessage)
                .backToList()
                .assertProcessIsNotShown(longProcessName);
    }
}
