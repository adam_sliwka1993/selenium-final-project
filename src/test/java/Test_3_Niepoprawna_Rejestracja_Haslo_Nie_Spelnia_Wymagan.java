import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pages.LoginPage;

public class Test_3_Niepoprawna_Rejestracja_Haslo_Nie_Spelnia_Wymagan extends SeleniumBaseTest {

    @DataProvider
    public Object[][] wrongPasswords() {
        return new Object[][]{
                {"automatyzacja122!", "Passwords must have at least one uppercase ('A'-'Z')."},
                {"Automatyzacja!", "Passwords must have at least one digit ('0'-'9')."},
                {"Automatyzacja122", "Passwords must have at least one non alphanumeric character."}
        };
    }

    @Test(dataProvider = "wrongPasswords")
    public void incorrectRegisterTest(String wrongPassword, String expErrorMessage) {
        new LoginPage(driver)
                .goToRegisterPage()
                .typeEmail("zanzibar@gmail.com")
                .typePassword(wrongPassword)
                .typeConfirmPassword(wrongPassword)
                .submitRegisterButtonWithFailure()
                .assertvalidationErrorsIsShown(expErrorMessage);
    }
}
